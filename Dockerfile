FROM node:21.6.0-alpine3.19


ARG NODE_ENV
ARG PORT


WORKDIR /home/nonroot

COPY ./package.json ./package-lock.json ./
RUN NODE_ENV=$NODE_ENV npm ci
COPY . .

RUN addgroup nonroot
RUN adduser --disabled-password --gecos "" --ingroup nonroot nonroot
RUN chown -R nonroot:nonroot /home/nonroot
USER nonroot

EXPOSE $PORT

CMD ["npm", "start"]
