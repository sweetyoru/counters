#!/usr/bin/env bash
set -e
[[ $TRACE ]] && set -x


echo `aws ecs describe-task-definition --task-definition  $ECS_TASK_DEFINITION --region $AWS_REGION` > input.json

echo $(cat input.json | jq '.taskDefinition') > input.json

echo $(cat input.json | jq  'del(.taskDefinitionArn)' | jq 'del(.revision)' | jq 'del(.status)' | jq 'del(.requiresAttributes)' | jq 'del(.compatibilities)' | jq 'del(.registeredAt)' | jq 'del(.registeredBy)') > input.json

aws ecs register-task-definition --cli-input-json file://input.json --region $AWS_REGION 
revision=$(aws ecs describe-task-definition --task-definition $ECS_TASK_DEFINITION --region $AWS_REGION | egrep "revision" | tr "/" " " | awk '{print $2}' | sed 's/"$//' | cut -d "," -f 1)

aws ecs update-service --cluster $ECS_CLUSTER --service $ECS_SERVICE  --task-definition $ECS_TASK_DEFINITION:$revision --region $AWS_REGION
